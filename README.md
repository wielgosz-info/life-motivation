# Life Motivation

A tiny project inspired by [Tim Urban's fantastic TED talk](https://www.ted.com/talks/tim_urban_inside_the_mind_of_a_master_procrastinator).
I created it to familiarize myself with [IndexedDB](https://developer.mozilla.org/en-US/docs/Web/API/IndexedDB_API/Using_IndexedDB) and [IntersectionObserver](https://developer.mozilla.org/en-US/docs/Web/API/Intersection_Observer_API).

Life Motivation is a project created with Chisel. Please check out Chisel documentation at [https://github.com/xfiveco/generator-chisel](https://github.com/xfiveco/generator-chisel).

