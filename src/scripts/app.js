import DataStorage from './modules/dataStorage';
import Questionnaire from './modules/questionnaire';
import Calendar from './modules/calendar';
import Tips from './modules/tips';
import Uncover from './modules/uncover';

/*
  Project: Life Calendar
  Author: Urszula Wielgosz
 */

class App {
  constructor() {
    document.addEventListener(
      'DOMContentLoaded',
      this.onDOMContentLoaded.bind(this),
    );

    this.onCalendarReady = this.onCalendarReady.bind(this);
    this.onQuestionnaireReady = this.onQuestionnaireReady.bind(this);
  }

  onDOMContentLoaded() {
    this.tips = new Tips();
    this.uncover = new Uncover();
    this.dataStorage = new DataStorage();

    this.dataStorage.on('ready', () => {
      this.questionnaire = new Questionnaire(this.dataStorage);
      this.calendar = new Calendar(this.dataStorage);

      this.questionnaire.on('ready', this.onQuestionnaireReady);
      this.questionnaire.on('criteriaChange', this.calendar.onCriteriaChange);
      this.questionnaire.on('dobChange', this.calendar.onDOBChange);

      this.calendar.on('updated', this.onCalendarReady);
    });
  }

  onQuestionnaireReady() {
    document
      .getElementsByTagName('body')[0]
      .classList.add('js-questionnaire-ready');

    this.uncover.resize();
    this.questionnaire.off('ready', this.onQuestionnaireReady);
  }

  onCalendarReady(total, past) {
    // wait until first birth date was provided
    if (past > 0) {
      document
        .getElementsByTagName('body')[0]
        .classList.add('js-calendar-ready');

      setTimeout(() => {
        this.uncover.resize();
      }, 50); // give browser time to update DOM
      this.calendar.off('updated', this.onCalendarReady);
    }
  }
}

const app = new App(); // eslint-disable-line no-unused-vars
