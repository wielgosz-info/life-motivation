import EventEmitter from 'event-emitter-es6';

import DataLoader from './dataLoader';

export default class DataStorage extends EventEmitter {
  constructor() {
    super();

    this.needsUpgrade = false;
    this.openDatabase();
  }

  openDatabase() {
    const request = window.indexedDB.open('LifeCalendarDatabase', 1);
    // request.onerror = this.onOpenError.bind(this);
    request.onsuccess = this.onOpenSuccess.bind(this);
    request.onupgradeneeded = this.onUpgradeNeeded.bind(this);
  }

  // onOpenError(ev) {
  //   console.error(ev.target.errorCode);
  // }

  onOpenSuccess(ev) {
    this.db = ev.target.result;

    if (!this.needsUpgrade) {
      this.emit('ready');
    }
  }

  onUpgradeNeeded(ev) {
    this.needsUpgrade = true;
    this.db = ev.target.result;
    const objectStore = this.db.createObjectStore('WHO', {
      keyPath: 'country',
    });

    objectStore.transaction.oncomplete = () => {
      this.seedDatabase().then(() => {
        this.emit('ready');
      });
    };
  }

  seedDatabase() {
    const promises = [];
    const loader = new DataLoader();

    return loader.requestWHOData().then(response => {
      const facts = Object.values(
        response.fact
          .map(fact => {
            const obj = {
              country: fact.dims.COUNTRY,
              value: {},
            };

            const key = {
              male: 'male',
              female: 'female',
              'both sexes': 'both',
            }[fact.dims.SEX.toLowerCase()];

            obj.value[key] = parseFloat(fact.Value);

            return obj;
          })
          .reduce((acc, fact) => {
            acc[fact.country] = Object.assign(
              Object.assign({ country: fact.country }, acc[fact.country]),
              fact.value,
            );
            return acc;
          }, {}),
      );

      const objectStore = this.db
        .transaction('WHO', 'readwrite')
        .objectStore('WHO');

      facts.forEach(fact => {
        promises.push(
          new Promise((resolve, reject) => {
            const addRequest = objectStore.add(fact);
            addRequest.onsuccess = resolve;
            addRequest.onerror = reject;
          }),
        );
      });

      return Promise.all(promises);
    });
  }

  get(store, key) {
    return new Promise((resolve, reject) => {
      const objectStore = this.db
        .transaction(store, 'readonly')
        .objectStore(store);

      const request = objectStore.get(key);
      request.onerror = () => {
        reject();
      };
      request.onsuccess = () => {
        resolve(request.result);
      };
    });
  }

  getAll(store, key = false) {
    return new Promise((resolve, reject) => {
      const objectStore = this.db
        .transaction(store, 'readonly')
        .objectStore(store);
      const results = [];

      objectStore.transaction.onerror = () => {
        reject();
      };

      objectStore[key ? 'openKeyCursor' : 'openCursor']().onsuccess = ev => {
        const cursor = ev.target.result;
        if (cursor) {
          results.push(cursor[key ? 'key' : 'value']);
          cursor.continue();
        } else {
          resolve(results);
        }
      };
    });
  }

  sanitizeGender(gender) {
    let sanitized = 'both';

    if (['male', 'female'].indexOf(gender) > -1) {
      sanitized = gender;
    }

    return sanitized;
  }

  getCountries() {
    return this.getAll('WHO', true);
  }

  getAverageYears(gender = 'both') {
    const sex = this.sanitizeGender(gender);

    return this.getAll('WHO').then(
      results =>
        results.reduce((acc, value) => acc + value[sex], 0) / results.length,
    );
  }

  getYears(location, gender = 'both') {
    const sex = this.sanitizeGender(gender);

    return this.get('WHO', location).then(result => result[sex]);
  }
}
