import EventEmitter from 'event-emitter-es6';

export default class Calendar extends EventEmitter {
  constructor(dataStorage) {
    super();

    this.dataStorage = dataStorage;

    this.selectors = {
      frame: '#calendar-frame',
    };

    this.classes = {
      item: 'c-calendar__item',
      past: 'c-calendar__item--past',
    };

    this.totalWeeks = 0;
    this.usedWeeks = 0;
    this.millisecondsInWeek = 1000 * 60 * 60 * 24 * 7;

    this.setupDOM();
    this.setupEvents();
  }

  setupDOM() {
    this.DOM = {
      frame: document.querySelector(this.selectors.frame),
    };
  }

  setupEvents() {
    this.onCriteriaChange = this.onCriteriaChange.bind(this);
    this.onDOBChange = this.onDOBChange.bind(this);
  }

  updateWeeks() {
    const clone = this.DOM.frame.cloneNode(false);

    // create new children off canvas
    for (let i = 0; i < this.totalWeeks; i += 1) {
      const li = document.createElement('li');
      li.classList.add(this.classes.item);
      clone.appendChild(li);
    }

    // add 'past' class as required off canvas
    Array.from(clone.children)
      .slice(0, this.usedWeeks)
      .forEach(item => {
        item.classList.add(this.classes.past);
      });

    // replace node and remember new reference
    this.DOM.frame.parentNode.replaceChild(clone, this.DOM.frame);
    this.DOM.frame = clone;

    this.emit('updated', this.totalWeeks, this.usedWeeks);
  }

  onCriteriaChange(gender, location) {
    let yearsPromise;
    if (location === 'any') {
      yearsPromise = this.dataStorage.getAverageYears(gender);
    } else {
      yearsPromise = this.dataStorage.getYears(location, gender);
    }

    yearsPromise.then(years => {
      this.totalWeeks = Math.round(years * 52);
      this.updateWeeks();
    });
  }

  onDOBChange(dob) {
    const today = new Date();
    const start = new Date(dob);

    this.usedWeeks = Math.round((today - start) / this.millisecondsInWeek);
    this.updateWeeks();
  }
}
