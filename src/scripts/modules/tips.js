export default class Tips {
  constructor() {
    this.selectors = {
      mark: '[js-tip]',
    };
    this.attributes = {
      target: 'js-tip',
    };
    this.classes = {
      open: 'is-open',
    };

    this.timeouts = [];

    this.setupDOM();
    this.setupEvents();
  }

  setupDOM() {
    this.DOM = {
      marks: Array.from(document.querySelectorAll(this.selectors.mark)),
    };
  }

  setupEvents() {
    this.onMarkEnter = this.onMarkEnter.bind(this);
    this.onMarkLeave = this.onMarkLeave.bind(this);
    this.onTipEnter = this.onTipEnter.bind(this);
    this.onTipLeave = this.onTipLeave.bind(this);

    this.DOM.marks.forEach(mark => {
      mark.addEventListener('mouseenter', this.onMarkEnter, false);
      mark.addEventListener('mouseleave', this.onMarkLeave, false);
    });
  }

  onMarkEnter(ev) {
    const mark = ev.target;
    const tip = this.getTip(mark);

    this.clearLeaveTimeout(mark);

    const top = mark.offsetTop + 0.8 * mark.offsetHeight;
    const left = mark.offsetLeft - tip.clientWidth + mark.offsetWidth / 2;

    tip.style.top = `${top}px`;
    tip.style.left = `${left}px`;
    tip.classList.add(this.classes.open);

    tip.addEventListener('mouseenter', this.onTipEnter, false);
    tip.addEventListener('mouseleave', this.onTipLeave, false);
  }

  onMarkLeave(ev) {
    const tip = this.getTip(ev.target);
    this.setLeaveTimeout(ev.target, tip);
  }

  setLeaveTimeout(mark, tip) {
    this.timeouts[this.DOM.marks.indexOf(mark)] = setTimeout(() => {
      tip.classList.remove(this.classes.open);
    }, 50);
  }

  clearLeaveTimeout(mark) {
    clearTimeout(this.timeouts[this.DOM.marks.indexOf(mark)]);
  }

  onTipEnter(ev) {
    const mark = this.getMark(ev.target);
    this.clearLeaveTimeout(mark);
  }

  onTipLeave(ev) {
    const mark = this.getMark(ev.target);
    this.setLeaveTimeout(mark, ev.target);
  }

  getTip(mark) {
    return document.querySelector(mark.getAttribute(this.attributes.target));
  }

  getMark(tip) {
    for (let i = 0; i < this.DOM.marks.length; i += 1) {
      if (this.getTip(this.DOM.marks[i]) === tip) {
        return this.DOM.marks[i];
      }
    }

    return null;
  }
}
