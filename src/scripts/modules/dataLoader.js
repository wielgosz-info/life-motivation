// data from:
// http://apps.who.int/gho/data/node.main.688?lang=en
// filtered to only show expectancy at birth as recorded in 2016
const WHO_DATA_URL = 'assets/data/who.json';

export default class DataLoader {
  requestJSON(url) {
    return new Promise((resolve, reject) => {
      const httpRequest = new XMLHttpRequest();

      httpRequest.onreadystatechange = () => {
        try {
          if (httpRequest.readyState === XMLHttpRequest.DONE) {
            if (httpRequest.status === 200) {
              const response = JSON.parse(httpRequest.responseText);
              resolve(response);
            } else {
              reject();
            }
          } else {
            // Not ready yet.
          }
        } catch (e) {
          reject();
        }
      };
      httpRequest.open('GET', url, true);
      httpRequest.send();
    });
  }

  requestWHOData() {
    return this.requestJSON(WHO_DATA_URL);
  }
}
