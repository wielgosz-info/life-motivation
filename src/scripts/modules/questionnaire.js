import EventEmitter from 'event-emitter-es6';

export default class Questionnaire extends EventEmitter {
  constructor(dataStorage) {
    super();

    this.dataStorage = dataStorage;

    this.selectors = {
      gender: '#gender',
      location: '#location',
      dob: '#dob',
      countriesOptgroup: '#countriesOptgroup',
    };

    this.setupDOM();
    this.setupEvents();
    this.populateCountries().then(() => {
      // trigger first calculation
      this.emitCriteriaChange();
    });
  }

  populateCountries() {
    return this.dataStorage.getCountries().then(countries => {
      const optgroup = document.querySelector(this.selectors.countriesOptgroup);
      const fragment = document.createDocumentFragment();

      countries.forEach(country => {
        fragment.appendChild(new Option(country));
      });

      optgroup.appendChild(fragment);

      this.emit('ready');
    });
  }

  setupDOM() {
    this.DOM = {
      gender: document.querySelector(this.selectors.gender),
      location: document.querySelector(this.selectors.location),
      dob: document.querySelector(this.selectors.dob),
    };
  }

  setupEvents() {
    this.emitCriteriaChange = this.emitCriteriaChange.bind(this);
    this.emitDOBChange = this.emitDOBChange.bind(this);

    this.DOM.gender.addEventListener('change', this.emitCriteriaChange);
    this.DOM.location.addEventListener('change', this.emitCriteriaChange);
    this.DOM.dob.addEventListener('change', this.emitDOBChange);
  }

  emitCriteriaChange() {
    this.emit('criteriaChange', this.DOM.gender.value, this.DOM.location.value);
  }

  emitDOBChange() {
    const dob = new Date(this.DOM.dob.value);

    if (dob.getFullYear() > 1900) {
      this.emit('dobChange', this.DOM.dob.value);
    }
  }
}
