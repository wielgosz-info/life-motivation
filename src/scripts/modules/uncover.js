export default class Uncover {
  constructor() {
    this.selectors = {
      container: '.c-uncover',
      item: '.c-uncover__item',
    };
    this.classes = {
      dummy: 'c-uncover__dummy',
      covered: 'is-covered',
    };

    this.setupDOM();
    this.setupEvents();
  }

  setupDOM() {
    this.DOM = {
      container: document.querySelector(this.selectors.container),
      items: Array.from(document.querySelectorAll(this.selectors.item)),
      dummies: [],
    };

    this.DOM.items.forEach((item, index) => {
      /* eslint-disable no-param-reassign */
      item.style.zIndex = this.DOM.items.length - index;

      if (index > 0) {
        const dummy = document.createElement('div');
        dummy.classList.add(this.classes.dummy);
        dummy.id = item.id ? `${item.id}` : `dummy-${index}`;
        item.id = '';
        this.DOM.dummies.push(dummy);
        this.DOM.container.insertBefore(dummy, item);
      }
      /* eslint-enable no-param-reassign */
    });
  }

  setupEvents() {
    this.onIntersectionChange = this.onIntersectionChange.bind(this);
    this.resize = this.resize.bind(this);

    this.observer = new IntersectionObserver(this.onIntersectionChange, {
      threshold: 0,
    });
    this.DOM.items.slice(0, this.DOM.items.length - 1).forEach(item => {
      this.observer.observe(item);
    });

    window.addEventListener('resize', this.resize, { passive: true });
  }

  onIntersectionChange(entries) {
    entries.forEach(entry => {
      const index = this.DOM.items.indexOf(entry.target);
      if (!entry.isIntersecting) {
        // item disappeared from the viewport
        this.DOM.items[index + 1].classList.remove(this.classes.covered);
        this.DOM.items[index + 1].style.top = '';
        this.DOM.dummies[index].style.display = 'none';
      } else {
        // item appears on the viewport
        this.DOM.dummies[index].style.display = 'block';
        this.DOM.items[index + 1].classList.add(this.classes.covered);
        this.DOM.items[index + 1].style.top = this.DOM.container.offsetTop;
      }
    });
  }

  resize() {
    this.DOM.items.slice(1, this.DOM.items.length).forEach((item, index) => {
      this.DOM.dummies[index].style.height = `${item.offsetHeight}px`;
    });
  }
}
